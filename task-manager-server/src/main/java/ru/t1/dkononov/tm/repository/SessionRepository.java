package ru.t1.dkononov.tm.repository;

import ru.t1.dkononov.tm.api.repository.ISessionRepository;
import ru.t1.dkononov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
